## Project 3, Section 3.2.1
### PPBL Week 5 | 14 March 2022

### How to get the public key hash of an address using `cardano-cli`:
```
cardano-cli address key-hash --payment-verification-key-file payment.vkey
```

Before we continue, let's get a feel for parameterized functions.

### Compile two different game instances:

```
Prelude Project03.ParameterizedGameCompiler> writeGameDemoScript01
Prelude Project03.ParameterizedGameCompiler> writeGameDemoScript02
```

### Create a Contract Address for each:

```
cd /project-03/output
cardano-cli address build --payment-script-file parameterized-game-01.plutus --testnet-magic 1097911063 --out-file parameterized-game-01.addr
cardano-cli address build --payment-script-file parameterized-game-02.plutus --testnet-magic 1097911063 --out-file parameterized-game-02.addr
```

### What do we have here? How can we use it?