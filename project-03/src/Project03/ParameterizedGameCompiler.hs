{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Project03.ParameterizedGameCompiler
    ( writeScriptGame01
    ) where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..))
import Codec.Serialise (serialise)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Ledger

import           Project03.ParameterizedGame

writeValidator :: FilePath -> Ledger.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Ledger.unValidatorScript

writeScriptGame01 :: IO (Either (FileError ()) ())
writeScriptGame01 = writeValidator "output/parameterized-game-01.plutus" $ Project03.ParameterizedGame.validator $ GameParam
  {
    gameHost = Ledger.PaymentPubKeyHash "22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3"
  , answer = 314159
  }
