{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

-- Optional, can Lock Play Tokens:
-- assetName: play
-- policyID: cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059

-- Step One "ValidateWithAnyToken"
-- If a user holds a PlutusPBLCourse01 token, then they can unlock any UTXO at this contract
-- assetName: PlutusPBLCourse01
-- policyID: 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c

-- Step Two "ValidateWithDefinedToken"
-- In datum, store the policyID of the token that can unlock the UTXO

module PpblEscrow.ValidateWithTokenInOutput where

import              Ledger              hiding (singleton)
import              Ledger.Typed.Scripts
import              Ledger.Value        as Value
import qualified    PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)

data AuthTokenInfo = AuthTokenInfo
  { authTokenSymbol :: !CurrencySymbol
  , authTokenName   :: !TokenName
  }

PlutusTx.makeLift ''AuthTokenInfo

-- The way this contract is built, it expects to see the Auth token at the CONTRACT address
-- This is due to the use of findOwnInput requiring the Auth Token

-- Instead, we want a contract that expects to see the Auth token in ANY input, and not just the CONTRACT's OWN input

{-# INLINEABLE mkValidator #-}
mkValidator :: AuthTokenInfo -> Integer -> Integer -> ScriptContext -> Bool
mkValidator atkn _ _ ctx =
    traceIfFalse "auth token missing from output" outputHasToken
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o
        _ -> traceError "expected exactly one output"

    outputHasToken :: Bool
    outputHasToken = valueOf (txOutValue ownOutput) (authTokenSymbol atkn) (authTokenName atkn) >= 1

data EscrowTypes

instance ValidatorTypes EscrowTypes where
    type DatumType EscrowTypes = Integer
    type RedeemerType EscrowTypes = Integer

typedValidator :: AuthTokenInfo -> TypedValidator EscrowTypes
typedValidator tkn =
  mkTypedValidator @EscrowTypes
    ($$(PlutusTx.compile [|| mkValidator ||]) `PlutusTx.applyCode` PlutusTx.liftCode tkn)
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = wrapValidator @Integer @Integer

validator :: AuthTokenInfo -> Validator
validator = validatorScript . typedValidator