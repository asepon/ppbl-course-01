{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module PpblEscrow.Bonfire.EscrowContractDraft where

import              Ledger              hiding (singleton)
import              Ledger.Typed.Scripts
import              Ledger.Value        as Value
import qualified    PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)
import              Prelude             (Show (..))
import PlutusTx

--
data BonfireEventEscrowDatum = BonfireEventEscrowDatum
  {
    organizerReference  :: !BuiltinByteString -- reference to our Bonfire DB
  , eventReference      :: !BuiltinByteString -- reference to our Bonfire DB
  , organizerAddress    :: !PubKeyHash
  , attendeeAddress     :: !PubKeyHash
  , eventCost           :: !Integer
  , eventStartTime      :: !POSIXTime -- how long?
  }

PlutusTx.unstableMakeIsData ''BonfireEventEscrowDatum

data EventAction = Cancel | Update | Complete | Dispute
  deriving Show

PlutusTx.makeIsDataIndexed ''EventAction [('Cancel, 0), ('Update, 1), ('Complete, 2), ('Dispute, 3)]
PlutusTx.makeLift ''EventAction

-- How do we guard against double-booking an event?
-- Does a token play a role in representing a single event?
-- Would that token be created at the time of event CREATION by ORGANIZER?
-- Or at the time of event BOOKING by ATTENDEE?

{-# INLINEABLE mkValidator #-}
mkValidator :: BonfireEventEscrowDatum -> EventAction -> ScriptContext -> Bool
mkValidator edatum action ctx =
  case action of
    Cancel    -> traceIfFalse "wrong signer"  (signedByAttendee (organizerAddress edatum)) ||  -- allow organizer to cancel
                 traceIfFalse "wrong signer"  (signedByAttendee (attendeeAddress edatum)) &&   -- allow attendee to cancel
                 traceIfFalse "too early" isDeadline -- check cancellation deadline, based on organizer's parameter
    Complete  -> True --traceIfFalse "error" signedByAttendee
                -- this is at the core of our escrow.
                -- who confirms that the event is complete, and how?
    Dispute   -> True --traceIfFalse "error" signedByAttendee
                -- for now, what can we promise in terms of dispute resolution?
                -- (someday, we'll be passing users along to Loxe.inc)
    Update    -> True --traceIfFalse "error" signedByAttendee
                -- who can update the event?
                -- check cancellation deadline, based on organizer's parameter

  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    signedByAttendee :: PubKeyHash -> Bool
    signedByAttendee pkh = txSignedBy info pkh
    
    -- time in Plutus is an interval (slots are) 
    isDeadline :: Bool 
    isDeadline = contains (from $ eventStartTime edatum) $ txInfoValidRange info 

data EscrowTypes

instance ValidatorTypes EscrowTypes where
    type DatumType EscrowTypes = BonfireEventEscrowDatum
    type RedeemerType EscrowTypes = EventAction

typedValidator :: TypedValidator EscrowTypes
typedValidator =
  mkTypedValidator @EscrowTypes
    $$(PlutusTx.compile [||mkValidator||])
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = wrapValidator @BonfireEventEscrowDatum @EventAction

validator :: Validator
validator = validatorScript typedValidator
