{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module PpblEscrow.Bonfire.Trace 
    ( test
    , mintTest 
    , startTest 
    , cancelTest
    ) where

import           Plutus.Trace.Emulator          as Emulator
                                                (activateContractWallet, waitNSlots, runEmulatorTraceIO', callEndpoint, EmulatorConfig(..))
import           Control.Monad                  (void)
import           PlutusTx.Prelude               as Plutus (($), Either(..))
import           Ledger.Value                   as Value (singleton)
import qualified Data.Map                       as Map
import qualified Ledger.Ada                     as Ada
import           Prelude                        (IO)
import           Data.Default                   (def)
import           Plutus.V1.Ledger.Value         (Value (..), TokenName (..))
import           Wallet.Emulator.Wallet         (Wallet (..), knownWallet, walletPaymentPubKeyHashes, mockWalletPaymentPubKeyHash)
import PlutusTx.Builtins.Internal
import PpblEscrow.Bonfire.Mint as Mint (endpoints)
import PpblEscrow.Bonfire.EscrowOffchain as Bonfire
import Ledger (PaymentPubKeyHash(unPaymentPubKeyHash), POSIXTime)
import Ledger.TimeSlot (slotToBeginPOSIXTime, slotToEndPOSIXTime)
 
alice :: Wallet 
alice = knownWallet 1

bob :: Wallet 
bob = knownWallet 2

--initialise wallets
dist :: Map.Map Wallet Value 
dist = Map.fromList [ (alice, Ada.lovelaceValueOf 100_000_000)
                            , (bob, Ada.lovelaceValueOf 100_000_000)
                            , (knownWallet 3, Ada.lovelaceValueOf 100_000_000)
                            , (knownWallet 4, Ada.lovelaceValueOf 100_000_000)
                            ]
     
emCfg :: EmulatorConfig
emCfg = EmulatorConfig (Left dist) def def 

tokenN :: TokenName 
tokenN = "event1"

testTime :: POSIXTime 
testTime = slotToEndPOSIXTime def 10

testEscrow :: StartParams 
testEscrow = StartParams "1" "1" (unPaymentPubKeyHash $ mockWalletPaymentPubKeyHash alice) (unPaymentPubKeyHash $ mockWalletPaymentPubKeyHash bob) 20000000 testTime 

test :: IO ()
test = 
    runEmulatorTraceIO' def emCfg $ do
        h1m <- activateContractWallet alice Mint.endpoints 
        h1b <- activateContractWallet alice Bonfire.endpoints
        h2b <- activateContractWallet bob Bonfire.endpoints
        void $ Emulator.waitNSlots 1
        callEndpoint @"mint" h1m tokenN
        void $ Emulator.waitNSlots 1
        callEndpoint @"start" h1b testEscrow
        void $ Emulator.waitNSlots 1
        callEndpoint @"cancel" h2b testEscrow
        void $ Emulator.waitNSlots 1

mintTest :: IO () 
mintTest = 
    runEmulatorTraceIO' def emCfg $ do 
        h1m <- activateContractWallet alice Mint.endpoints
        void $ Emulator.waitNSlots 1 
        callEndpoint @"mint" h1m tokenN 
        void $ Emulator.waitNSlots 1

startTest :: IO () 
startTest = 
    runEmulatorTraceIO' def emCfg $ do 
        h1m <- activateContractWallet alice Mint.endpoints
        h1b <- activateContractWallet alice Bonfire.endpoints 
        void $ Emulator.waitNSlots 1 
        callEndpoint @"mint" h1m tokenN 
        void $ Emulator.waitNSlots 1
        callEndpoint @"start" h1b testEscrow 
        void $ Emulator.waitNSlots 1

cancelTest :: IO () 
cancelTest = 
    runEmulatorTraceIO' def emCfg $ do 
        h1m <- activateContractWallet alice Mint.endpoints
        h1b <- activateContractWallet alice Bonfire.endpoints 
        h2b <- activateContractWallet bob Bonfire.endpoints 
        void $ Emulator.waitNSlots 1 
        callEndpoint @"mint" h1m tokenN 
        void $ Emulator.waitNSlots 1
        callEndpoint @"start" h1b testEscrow 
        void $ Emulator.waitNSlots 1
        callEndpoint @"cancel" h1b testEscrow
        void $ Emulator.waitNSlots 1



