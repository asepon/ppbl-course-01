## 2.3.3 Exploring Context

In a Plutus validation script, the purpose of the validator is to determine whether or not a pending transaction is valid.

`ScriptContext` gives the validator the ability to "look inside" of a pending transaction get some information about it: in other words, to see the actual "context" in which the script is being run. To get started, let's take a quick look [at the official documentation](https://playground.plutus.iohkdev.io/doc/haddock/plutus-ledger-api/html/Plutus-V1-Ledger-Contexts.html). Then, we'll review two simple examples of how context can be used in a minting transaction.

For each example, compile the code in `/src/Project02`, following steps as shown in [Part 2.2.3](https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01/-/tree/master/project-02)

- Extended Compiler: `/src/Project02/SimplePlutusMintingCompiler.hs`
- Check Token Name source code: `/src/Project02/MintRedeemerTokenName.hs`
- Check Minted Amount source code: `/src/Project02/MintRedeemerMintedAmount.hs`

## Example C: Check Token Name

#### Step C1
```
cardano-cli transaction policyid --script-file context-minting-script-tn.plutus > context-minting-script-tn.id
```

#### Step C2
Set variables in bash.
```
SENDER=$(cat ...path-to/base.addr)
SENDERKEY= path to payment.skey for this address
TXIN= hash and id
COLLATERAL= hash and id
POLICYID= from Step C1
TOKENNAME=""
MINTAMOUNT=
SCRIPTFILE=" ... path to context-minting-script-tn.plutus"
```

#### Step C3

Note: try minting a token where $TOKENNAME is anything other than "play" to see our error message.

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-out $SENDER+2000000+"$MINTAMOUNT $POLICYID.$TOKENNAME" \
--change-address $SENDER \
--mint "$MINTAMOUNT $POLICYID.$TOKENNAME" \
--mint-script-file $SCRIPTFILE \
--mint-redeemer-value 1 \
--tx-in-collateral $COLLATERAL \
--protocol-params-file protocol.json \
--out-file mint-play.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file mint-play.raw \
--out-file mint-play.signed

cardano-cli transaction submit \
--tx-file mint-play.signed \
--testnet-magic 1097911063
```
## Example D: Check Minted amount

#### Step D1
```
cardano-cli transaction policyid --script-file context-minting-script-amount.plutus > context-minting-script-amount.id
```

#### Step D2
Set variables in bash.
```
SENDER=$(cat ...path-to/base.addr)
SENDERKEY= path to payment.skey for this address
TXIN= hash and id
COLLATERAL= hash and id
POLICYID= from Step D1
TOKENNAME=""
MINTAMOUNT=5
SCRIPTFILE=" ... path to context-minting-script-amount.plutus"
```

#### Step D3

Note: try minting a token where $MINTAMOUNT is anything other than 5 to see our error message.

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-out $SENDER+2000000+"$MINTAMOUNT $POLICYID.$TOKENNAME" \
--change-address $SENDER \
--mint "$MINTAMOUNT $POLICYID.$TOKENNAME" \
--mint-script-file $SCRIPTFILE \
--mint-redeemer-value 1 \
--tx-in-collateral $COLLATERAL \
--protocol-params-file protocol.json \
--out-file mint-five.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file mint-five.raw \
--out-file mint-five.signed

cardano-cli transaction submit \
--tx-file mint-five.signed \
--testnet-magic 1097911063
```

## Your Turn!
Write a Minting Policy that combines token name validation and minting quantity validation to create whatever policy you'd like. As you do, consider the current limitations. For example, does Context validation provide enough logic to create "NFTs"?

You can also choose to add redeemers to your policy.

### Steps
1. In `/src/Project02`, create a new file called `MyNewCustomMintingPolicy.hs` or whatever you'd like
2. Into this new file, copy the contents of one of the other examples in `/src/Project02`
3. Edit this file to create the minting policy of your choice - combining any parts you've seen in `Project02`.
4. Using the other functions as a template, write a new function in `SimplePlutusMintingCompiler.hs` that creates a compiled `.plutus` script from your new, custom policy.
5. Build a transaction that uses this newly compiled script to mint your very own tokens!

### Extension:
Parameterized minting functions: what we really want is a family of functions. If there's time at this week's live coding sessions, we can take a first look at parameterized minting functions.